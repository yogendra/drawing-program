# Drawing Program

This is a simple console based ascii drawing program.

## Documentation Convention

* All the code and scripts are in a `monospaced` or fixed-width fonts.
* All UNIX/Linux/OSX commands are prefixed with `$`.
* All Windows commands are prefixed with `>`.

## How to build

Simple clone-cd-build should just work. You will require:

1. Git
1. Java 1.8+

* On Unix/Linux/OSX

        $ git clone git@bitbucket.org:yogendra/drawing-program.git drawing-program
        $ cd drawing-program
        $ ./gradlew clean assembleDist

* On Windows

        > git clone git@bitbucket.org:yogendra/drawing-program.git drawin-program
        > cd drawing-program
        > gradlew clean assembleDist


## How to Run 

You should have installable pacakges in `build/distributions` directory.

* On Unix/Linux/OSX

        $ build/distributions/drawing-program.tar`

* On Windows
    
        build\distributions\drawing-program.zip

Exract the package in a directory and run the program startup script
* On Unix/Linux/OSX 
        
        bin/drawing-program

* On Windows
        
        bin\drawind-program

Alternatively, You can run extracted version of these packages, in-place, using following commands

* On Unix/Linux/OSX
    
        $ ./gradlew installDist
        $ build/install/drawing-program/bin/drawing-program

* On Windows
    
        > gradlew installDist
        > build\install\drawing-program\bin\drawing-program

## How to Run Tests

**Unit Test**

* On Unix/Linux/OSX
    
        $ ./gradlew test

* On Windows
    
        > gradlew test

**Integration Test**

* On Unix/Linux/OSX
    
        $ ./gradlew itest

* On Windows
        
        > gradlew itest

## Run Sonarqube

Create a `build.properties` file with following properties:

    sonar.host.url=http://localhost:9000
    sonar.login=bsdjkashjkdhshdk82342483

Get the exact values from your sonarqube instance. You can run analysis on code using command below.

* On Unix/Linux/OSX

        ./gradlew sonarqube

* on Windows

        gradlew sonarquve

## Basic System Flow

This application uses 4 kinds of objects. 

* A Shell
* A Shell Processor
* Command
* Action

Interactions between these is shown below

![Application FLow](docs/diagram/app-flow.png)

User interacts with the shell. Shell handles all the user interactions and passes the commands to processor. Processor takes the command and parses it. After this it executes the relevant command. Command is responsible for parsing and validating argumaents and creating action. This action is send back to the processor. Processor based on the kind of action, executes it. This action can be a Canvas Action (Line, Rectangle or Bucket Fill) or a Shell Action (Create Canvase or Quit).

