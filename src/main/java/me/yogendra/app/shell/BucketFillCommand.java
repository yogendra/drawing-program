package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import me.yogendra.app.draw.BucketFillAction;
import me.yogendra.app.draw.Point;
import org.slf4j.Logger;

public class BucketFillCommand extends BaseCommand<CanvasAction> {

  private static final Logger logger = getLogger(BucketFillCommand.class);

  public BucketFillCommand() {
    super("B");
  }

  public CanvasAction execute(String params) {
    ParameterHelper helper = paramHelper(params);
    Point point = helper.readPoint();
    char color = helper.readColor();
    logger.debug("Create fill action Point:{}, color:{}", point, color);
    return new BucketFillAction(point, color);
  }


}
