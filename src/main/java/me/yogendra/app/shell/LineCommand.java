package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import me.yogendra.app.draw.LineAction;
import me.yogendra.app.draw.Point;
import org.slf4j.Logger;

public class LineCommand extends BaseCommand<CanvasAction> {

  private static final Logger logger = getLogger(LineCommand.class);

  public LineCommand() {
    super("L");
  }

  public CanvasAction execute(String params) {
    ParameterHelper helper = paramHelper(params);
    Point start = helper.readPoint();
    Point end = helper.readPoint();
    logger.debug("Creating line action with start: {}, end: {}", start, end);
    return new LineAction(start, end);
  }


}
