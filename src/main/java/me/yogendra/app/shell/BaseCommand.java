package me.yogendra.app.shell;

public abstract class BaseCommand<T extends Action> implements Command<T> {

  private final String name;

  BaseCommand(String name) {
    this.name = name;
  }

  @Override
  public String getName() {
    return name;
  }


  protected ParameterHelper paramHelper(String params) {
    return new ParameterHelper(params);
  }
}
