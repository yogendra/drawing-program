package me.yogendra.app.shell;

class ShellProcessingException extends RuntimeException {

  private ShellProcessingException(String message) {
    super(message);
  }

  static ShellProcessingException commandNotFound() {
    return new ShellProcessingException("Command not found");
  }

  static ShellProcessingException emptyCommand() {
    return new ShellProcessingException("Empty command not allowed");
  }
  static ShellProcessingException missingArgument() {
    return new ShellProcessingException("No arguments provided");
  }


  static ShellProcessingException processorShutdown() {
    return new ShellProcessingException("Shell is already shutdown");
  }

  static ShellProcessingException unsupportedAction() {
    return new ShellProcessingException("An unsupported actions encountered.");
  }

  static ShellProcessingException invalidParameter(String s) {
    return new ShellProcessingException("Invalid Parameter: " + s);
  }

  static ShellProcessingException missingParameter(String helpText) {
    return new ShellProcessingException("Missing Parameter: " + helpText);
  }

  public static ShellProcessingException canvasNotCreated() {
    return new ShellProcessingException("Canvas not created. Create a canvas first");
  }
}
