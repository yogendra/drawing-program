package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import me.yogendra.app.draw.Canvas;
import org.slf4j.Logger;

public class CreateCanvasCommand extends BaseCommand<ShellAction> {

  private static final Logger logger = getLogger(CreateCanvasCommand.class);

  public CreateCanvasCommand() {
    super("C");
  }


  public ShellAction execute(String params) {
    ParameterHelper param = paramHelper(params);
    int width = param.readPositiveNumber();
    int height = param.readPositiveNumber();
    logger.debug("Creating canvas ({}x{})", width,height);

    return (ShellProcessor p) -> {
      Canvas canvas = new Canvas(width, height);
      p.setCanvas(canvas);
    };
  }
}
