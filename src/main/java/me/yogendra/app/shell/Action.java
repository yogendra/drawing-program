package me.yogendra.app.shell;

public interface Action<T> {

  void executeOn(T t);
}
