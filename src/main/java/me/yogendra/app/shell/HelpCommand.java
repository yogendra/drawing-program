package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import java.io.InputStream;
import java.util.Scanner;
import org.slf4j.Logger;

public class HelpCommand extends BaseCommand<ShellAction> {

  static final String COMMAND_NAME = "H";
  private static final Logger logger = getLogger(HelpCommand.class);


  public HelpCommand() {
    super(COMMAND_NAME);
  }

  public ShellAction execute(String params) {
    return s -> s.print(helpMessage());
  }

  private String help;

  private String helpMessage() {
    if (help == null) {
      logger.debug("Loading Help");
      help = loadHelp();
    }

    return help;

  }

  private String loadHelp() {
    InputStream helpFile = getClass().getResourceAsStream("/help.txt");
    StringBuilder buffer = new StringBuilder();
    try (Scanner s = new Scanner(helpFile)) {
      while (s.hasNext()) {
        buffer.append(s.nextLine());
        buffer.append("\n");
      }
    }
    return buffer.toString();
  }
}
