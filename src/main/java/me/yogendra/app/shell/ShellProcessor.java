package me.yogendra.app.shell;

import static me.yogendra.app.shell.ShellProcessingException.canvasNotCreated;
import static me.yogendra.app.shell.ShellProcessingException.commandNotFound;
import static me.yogendra.app.shell.ShellProcessingException.emptyCommand;
import static me.yogendra.app.shell.ShellProcessingException.processorShutdown;
import static me.yogendra.app.shell.ShellProcessingException.unsupportedAction;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Consumer;
import java.util.stream.Stream;
import me.yogendra.app.draw.Canvas;
import org.slf4j.Logger;
import org.slf4j.MDC;

public class ShellProcessor {

  private static final Logger logger = getLogger(ShellProcessor.class);

  private static final String COMMAND_ARG_SEPARATOR = " ";


  private boolean shutdown;
  private Map<String, Command> commands;
  private Consumer<String> out;
  private Canvas canvas;

  public ShellProcessor(Consumer<String> out, Command... commands) {
    this.shutdown = false;
    this.commands = new HashMap<>();
    this.out = out;

    Stream.of(commands)
        .forEach(command -> this.commands.put(command.getName(), command));
  }

  void setCanvas(Canvas canvas) {
    this.canvas = canvas;
  }

  void shutdown() {
    this.shutdown = true;
  }

  void print(String p) {
    out.accept(p);
  }

  boolean execute(String commandLine) {
    logger.debug("Start of execution // shutdown: {}", shutdown);
    if (shutdown) {
      logger.warn("Execution after shutdown");
      throw processorShutdown();
    }
    if (commandLine == null || commandLine.trim().equals("")) {
      logger.warn("Empty command given");
      throw emptyCommand();
    }
    logger.debug("Command Line: {}", commandLine);
    executeCommand(commandLine);
    if (!shutdown) {
      printCanvas();
    }
    logger.debug("End of execution // shutdown: {}", shutdown);
    return !shutdown;
  }

  private void printCanvas() {
    if (canvas != null) {
      logger.debug("Rendering canvas");
      canvas.render(this.out);
    }
  }

  private void executeCommand(String commandLine) {
    String[] parts = commandLine.split(COMMAND_ARG_SEPARATOR, 2);
    String name = parts[0];
    String args = "";
    if (parts.length == 2) {
      args = parts[1];
    }
    logger.debug("command: {}, args: {}", name, args);
    MDC.put("command", name);
    Command command = commands.get(name);
    if (command == null) {
      throw commandNotFound();
    }
    Action action = command.execute(args);
    logger.debug("command executed. action obtained");
    executeAction(action);
    MDC.remove("command");
  }

  private void executeAction(Action action) {
    if (action instanceof ShellAction) {
      executeShellAction((ShellAction) action);
    } else if (action instanceof CanvasAction) {
      executeCanvasAction((CanvasAction) action);
    } else {
      throw unsupportedAction();
    }
  }

  private void executeCanvasAction(CanvasAction action) {
    if (this.canvas != null) {
      logger.debug("Executing canvas action: {}", action.getClass().getSimpleName());
      action.executeOn(this.canvas);
    } else {
      logger.warn("Canvas not created");
      throw canvasNotCreated();
    }
  }

  private void executeShellAction(ShellAction action) {
    logger.debug("Executing shell action: {}", action.getClass().getSimpleName());
    action.executeOn(this);
    logger.debug("Finished executing shell action");
  }
}

