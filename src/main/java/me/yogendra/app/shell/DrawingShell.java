package me.yogendra.app.shell;

import static java.lang.String.format;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.function.Consumer;
import java.util.function.Supplier;
import me.yogendra.app.draw.CanvasException;
import org.slf4j.Logger;
import org.slf4j.MDC;

public class DrawingShell {

  static final String PROMPT = "enter command: ";
  private static final Logger logger = getLogger(DrawingShell.class);
  private Supplier<String> in;
  private Consumer<String> out;
  private ShellProcessor processor;
  private boolean shouldContinue = true;


  public DrawingShell(Supplier<String> in, ShellProcessor processor,
      Consumer<String> out) {
    this.in = in;
    this.processor = processor;
    this.out = out;
  }

  public void start() {
    logger.info("Starting shell");
    while (shouldContinue) {
      readAndProcess();
    }
    logger.info("Shutting down");
  }

  private void readAndProcess() {
    logger.info("Waiting for user in");
    out.accept(PROMPT);
    String input = in.get();
    processInput(input);
  }

  private void processInput(String input) {
    MDC.put("input", input);
    logger.debug("input: {}", input);
    invokeProcessor(input);
    logger.info("processed");
    MDC.remove("input");
  }


  private void invokeProcessor(String command) {
    try {
      shouldContinue = processor.execute(command);
      logger.debug("shouldContinue: {}", shouldContinue);
    } catch (CanvasException | ShellProcessingException ex) {
      logger.warn("Unable to invokeProcessor user command");
      out.accept(format("Error in executing %s: %s%n", command, ex.getMessage()));
    }
  }
}

