package me.yogendra.app.shell;

public interface Command<T extends Action> {

  String getName();

  T execute(String params);
}
