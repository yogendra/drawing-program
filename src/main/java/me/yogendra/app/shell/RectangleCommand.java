package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import me.yogendra.app.draw.Point;
import me.yogendra.app.draw.RectangleAction;
import org.slf4j.Logger;

public class RectangleCommand extends BaseCommand<CanvasAction> {

  private static final Logger logger = getLogger(RectangleCommand.class);

  public RectangleCommand() {
    super("R");
  }

  public CanvasAction execute(String params) {
    logger.debug("Start execute");
    ParameterHelper helper = paramHelper(params);
    Point start = helper.readPoint();
    Point end = helper.readPoint();
    logger.debug("rectangle action: start: {} end: {}", start, end);
    return new RectangleAction(start, end);

  }
}
