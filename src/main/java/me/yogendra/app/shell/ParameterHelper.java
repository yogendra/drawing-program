package me.yogendra.app.shell;

import static me.yogendra.app.shell.ShellProcessingException.invalidParameter;
import static me.yogendra.app.shell.ShellProcessingException.missingArgument;
import static me.yogendra.app.shell.ShellProcessingException.missingParameter;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.NoSuchElementException;
import java.util.Scanner;
import me.yogendra.app.draw.Point;
import org.slf4j.Logger;

class ParameterHelper {
  private static final Logger logger = getLogger(ParameterHelper.class);

  private final Scanner scanner;

  ParameterHelper(String param) {
    if (param == null) {
      throw missingArgument();
    }
    this.scanner = new Scanner(param);
  }

  private void closeIfDone() {
    if (!scanner.hasNext()) {
      logger.debug("Closing scanner");
      this.scanner.close();
    }
  }

  Point readPoint() {
    int x = readPositiveNumber();
    int y = readPositiveNumber();
    return Point.point(x, y);
  }

  int readPositiveNumber() {
    try {
      int i = scanner.nextInt();
      closeIfDone();
      if (i < 1) {
        throw invalidParameter("Expected a positive numbers (> 0)");
      }
      return i;
    } catch (IllegalStateException | NoSuchElementException e) {
      throw missingParameter("Expected a number");
    }
  }

  char readColor() {
    try {
      String in = scanner.next();
      closeIfDone();
      if (in.length() == 0 || in.length() > 1) {
        throw invalidParameter("Color should be single character");
      }
      return in.charAt(0);

    } catch (IllegalStateException | NoSuchElementException e) {
      throw missingParameter("Expected a color");
    }
  }
}
