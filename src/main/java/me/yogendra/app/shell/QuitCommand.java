package me.yogendra.app.shell;

import static org.slf4j.LoggerFactory.getLogger;

import org.slf4j.Logger;

public class QuitCommand extends BaseCommand<ShellAction> {

  static final String COMMAND_NAME = "Q";
  private static final Logger logger = getLogger(QuitCommand.class);

  public QuitCommand() {
    super(COMMAND_NAME);
  }

  public ShellAction execute(String params) {
    logger.debug("Start execute");
    return ShellProcessor::shutdown;
  }
}
