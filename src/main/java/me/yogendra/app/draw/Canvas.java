package me.yogendra.app.draw;

import static java.lang.String.format;
import static java.util.stream.IntStream.range;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.slf4j.Logger;

public class Canvas {
  private static final Logger logger = getLogger(Canvas.class);

  private static final String HORIZONTAL_LINE = "-";
  private static final String VERTICAL_LINE = "|";
  public static final int MAX_HEIGHT = 250;
  public static final int MAX_WIDTH = 250;
  private int borderWidth;
  private int width;
  private int height;
  private char[][] view;

  public Canvas(int width, int height) {
    if (width < 1 || width > MAX_WIDTH) {
      throw CanvasException.invalidSize("Width should be between 1 and " + MAX_WIDTH);
    }
    if (height < 1 || height > MAX_HEIGHT) {
      throw CanvasException.invalidSize("Height should be between 1 and " + MAX_HEIGHT);
    }
    this.width = width;
    this.height = height;
    this.borderWidth = 1 + width + 1;
    this.view = new char[height][width];
    Stream.of(view)
        .forEach(row -> Arrays.fill(row, ' '));
  }

  public int getWidth() {
    return width;
  }


  public int getHeight() {
    return height;
  }


  public void render(Consumer<String> out) {
    renderHorizontalBorder(out);
    renderMiddle(out);
    renderHorizontalBorder(out);
  }

  private void renderMiddle(Consumer<String> out) {
    int startRow = 0;
    int endRow = height;
    range(startRow, endRow).forEach(r -> printRow(r, out));
  }

  private void renderHorizontalBorder(Consumer<String> out) {
    String line = range(0, borderWidth).mapToObj(i -> HORIZONTAL_LINE)
        .collect(Collectors.joining());
    out.accept(line);
    out.accept(System.lineSeparator());
  }

  private void printRow(int rowNum, Consumer<String> out) {
    out.accept(VERTICAL_LINE);
    out.accept(new String(view[rowNum]));
    out.accept(VERTICAL_LINE);
    out.accept(System.lineSeparator());
  }

  public void setPixel(Point point, char o) {
    checkPointBounds(point);
    logger.debug("Paint {} with {}", point, o);
    view[point.y - 1][point.x - 1] = o;
  }

  public char getPixel(Point point) {
    checkPointBounds(point);
    return view[point.y - 1][point.x - 1];
  }

  private void checkPointBounds(Point point) {
    if (point.x < 1 || point.x > width || point.y < 1 || point.y > height) {
      throw new CanvasException(
          format("A point should be within canvas ( 0 < x <= %s, 0< y <= %s)", width, height));
    }
  }
}
