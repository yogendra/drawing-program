package me.yogendra.app.draw;

import static me.yogendra.app.draw.CanvasException.missingArgument;
import static me.yogendra.app.draw.Point.point;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.ArrayDeque;
import java.util.Deque;
import me.yogendra.app.shell.CanvasAction;
import org.slf4j.Logger;

public class BucketFillAction implements CanvasAction {

  private static final Logger logger = getLogger(BucketFillAction.class);


  private Point point;
  private char color;

  public BucketFillAction(Point point, char color) {
    if (point == null) {
      throw missingArgument("Point argument not given");
    }
    if (color == 0) {
      throw missingArgument("Color argument not given");
    }

    this.point = point;
    this.color = color;
  }

  @Override
  public void executeOn(Canvas canvas) {
    FillAlgorithm algorithm = new FillAlgorithm(point, color, canvas);
    algorithm.paint();
  }

  static class FillAlgorithm {

    final Deque<Point> points;
    final Canvas canvas;
    final boolean[][] painted;
    final char sourceColor;
    final char color;
    private Point current;

    FillAlgorithm(Point point, char color, Canvas canvas) {
      this.canvas = canvas;
      this.color = color;
      this.painted = new boolean[canvas.getWidth()][canvas.getHeight()];
      this.sourceColor = canvas.getPixel(point);
      this.points = new ArrayDeque<>();
      points.push(point);
    }

    public void paint() {
      while (!points.isEmpty()) {
        current = points.pop();
        logger.debug("Processing point: {}", current);
        if (shouldPaint()) {
          logger.debug("Painting point: {}", current);
          doPaint();
        }
      }
    }

    private boolean shouldPaint() {
      char currentColor = canvas.getPixel(current);
      boolean notPainted = !painted[current.x - 1][current.y - 1];
      boolean sameColor = currentColor == sourceColor;
      boolean shouldPaint = notPainted && sameColor;

      logger.debug(
          "Point: {}, shouldPaint: {}, notPainted: {}, sameColor: {}, currentColor: [{}], sourceColor: [{}]",
          current, shouldPaint, notPainted, sameColor, currentColor, sourceColor);
      return shouldPaint;
    }

    private void doPaint() {
      logger.debug("Painting: {}", current);
      canvas.setPixel(current, color);
      painted[current.x - 1][current.y - 1] = true;

      if (current.x != 1) {
        points.push(point(current.x - 1, current.y));
      }
      if (current.x < canvas.getWidth()) {
        points.push(point(current.x + 1, current.y));
      }
      if (current.y != 1) {
        points.push(point(current.x, current.y - 1));
      }
      if (current.y < canvas.getHeight()) {
        points.push(point(current.x, current.y + 1));
      }

    }
  }
}
