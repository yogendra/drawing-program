package me.yogendra.app.draw;

public class CanvasException extends RuntimeException {

  CanvasException(String message) {
    super(message);

  }

  public static CanvasException invalidSize(String info) {
    return new CanvasException("Invalid Size: " + info);
  }

  public static CanvasException unsupportedOperation(String info){
    return new CanvasException("Operation Not Supported: " + info);
  }

  public static CanvasException missingArgument(String help) {
    return new CanvasException("Missing Arguments: " + help);
  }
}
