package me.yogendra.app.draw;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static me.yogendra.app.draw.CanvasException.missingArgument;
import static me.yogendra.app.draw.CanvasException.unsupportedOperation;
import static me.yogendra.app.draw.Point.point;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.stream.IntStream;
import me.yogendra.app.shell.CanvasAction;
import org.slf4j.Logger;

public class LineAction implements CanvasAction {
  private static final Logger logger = getLogger(LineAction.class);

  private Point start;
  private Point end;

  public LineAction(Point start, Point end) {
    if(start == null){
      throw missingArgument("Start point missing");
    }
    if(end == null){
      throw missingArgument("End point missing");
    }
    this.start = start;
    this.end = end;
  }

  @Override
  public void executeOn(Canvas canvas) {

    boolean isHorizontal = start.y == end.y;
    boolean isVertical = start.x == end.x;

    if (isHorizontal) {
      logger.debug("Horizontal line from {} to {}", start, end);
      stream(start.x, end.x)
          .mapToObj(x -> point(x, start.y))
          .forEach(point -> canvas.setPixel(point, 'x'));
    } else if (isVertical) {
      logger.debug("Vertical line from {} to {}", start, end);
      stream(start.y, end.y)
          .mapToObj(y -> point(start.x, y))
          .forEach(point -> canvas.setPixel(point, 'x'));
    } else {
      throw unsupportedOperation("Only Horizontal and Vertical Lines are supported");
    }
  }

  private IntStream stream(int n1, int n2) {
    return IntStream.rangeClosed(min(n1, n2), max(n1, n2));
  }
}
