package me.yogendra.app.draw;

import static java.lang.String.format;

import java.util.Objects;

public class Point {

  public final int x;
  public final int y;

  private Point(int x, int y) {
    this.x = x;
    this.y = y;
  }

  @Override
  public String toString() {
    return format("(%d,%d)", x, y);
  }

  public static Point point(int x, int y) {
    return new Point(x, y);
  }

  @Override
  public boolean equals(Object obj) {
    if (!(obj instanceof Point)) {
      return false;
    }
    Point p = (Point) obj;

    return Objects.equals(this.x, p.x)
        && Objects.equals(this.y, p.y);
  }

  @Override
  public int hashCode() {
    return Objects.hash("x",x,"y",y);
  }
}
