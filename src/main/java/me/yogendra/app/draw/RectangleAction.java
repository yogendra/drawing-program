package me.yogendra.app.draw;

import static me.yogendra.app.draw.CanvasException.missingArgument;
import static me.yogendra.app.draw.Point.point;
import static org.slf4j.LoggerFactory.getLogger;

import me.yogendra.app.shell.CanvasAction;
import org.slf4j.Logger;

public class RectangleAction implements CanvasAction {
  private static final Logger logger = getLogger(RectangleAction.class);

  private Point start;
  private Point end;

  public RectangleAction(Point start, Point end) {
    if(start == null){
      throw missingArgument("Start point missing");
    }
    if(end == null){
      throw missingArgument("End point missing");
    }

    this.start = start;
    this.end = end;
  }

  @Override
  public void executeOn(Canvas canvas) {
    Point topRight = point(end.x, start.y);
    Point bottomLeft = point(start.x, end.y);

    drawLine(canvas, start, topRight); // top
    drawLine(canvas, topRight, end); // right
    drawLine(canvas, bottomLeft, end); // bottom
    drawLine(canvas, start, bottomLeft); // left
  }
  private void drawLine(Canvas canvas, Point start, Point end){
    logger.debug("Draw Line: {} -> {}", start, end);
    LineAction line = new LineAction(start, end);
    line.executeOn(canvas);

  }


}
