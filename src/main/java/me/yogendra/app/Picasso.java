package me.yogendra.app;

import java.io.Console;
import java.util.function.Consumer;
import java.util.function.Supplier;
import me.yogendra.app.shell.BucketFillCommand;
import me.yogendra.app.shell.Command;
import me.yogendra.app.shell.CreateCanvasCommand;
import me.yogendra.app.shell.DrawingShell;
import me.yogendra.app.shell.HelpCommand;
import me.yogendra.app.shell.LineCommand;
import me.yogendra.app.shell.QuitCommand;
import me.yogendra.app.shell.RectangleCommand;
import me.yogendra.app.shell.ShellProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Picasso {

  private static final Logger logger = LoggerFactory.getLogger(Picasso.class);
  private Supplier<String> in;
  private Consumer<String> out;


  public static void main(String[] args) {
    Picasso picasso = new Picasso();
    picasso.run();
  }

  void run() {
    setupInputAndOutput();
    ShellProcessor processor = new ShellProcessor(out, commands());
    DrawingShell shell = new DrawingShell(in, processor, out);
    shell.start();
  }

  private void setupInputAndOutput() {
    Console console = System.console();
    if (console == null) {
      in = () -> "Q";
      out = logger::debug;
    } else {
      in = console::readLine;
      out = line -> console.printf("%s", line);
    }
  }

  private Command[] commands() {
    return new Command[]{
        new CreateCanvasCommand(),
        new HelpCommand(),
        new QuitCommand(),
        new LineCommand(),
        new RectangleCommand(),
        new BucketFillCommand()
    };
  }


}
