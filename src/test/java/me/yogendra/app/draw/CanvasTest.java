package me.yogendra.app.draw;

import static me.yogendra.app.TestHelper.testData;
import static me.yogendra.app.draw.Canvas.MAX_HEIGHT;
import static me.yogendra.app.draw.Canvas.MAX_WIDTH;
import static me.yogendra.app.draw.Point.point;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CanvasTest {

  private Canvas canvas;

  @Before
  public void setUp() {
    this.canvas = new Canvas(20,4);
  }

  @Test
  public void createCanvas() {
    assertNotNull(canvas);
  }

  @Test(expected = CanvasException.class)
  public void failsToCreateCanvasOfZeroWidth(){
    new Canvas(0,MAX_HEIGHT);
  }
  @Test(expected = CanvasException.class)
  public void failsToCreateCanvasWiderThanMax(){
    new Canvas(MAX_WIDTH  + 1, MAX_HEIGHT);
  }
  @Test(expected = CanvasException.class)
  public void failsToCreateCanvasOfZeroHeight(){
    new Canvas(MAX_WIDTH,0);
  }
  @Test(expected = CanvasException.class)
  public void failsToCreateCanvasTallerThanMax(){
    new Canvas(MAX_WIDTH, MAX_HEIGHT+1);
  }  @Test
  public void shouldProvideWidthAndHeight() {
    assertThat("Width setting failed", canvas.getWidth(), is(20));
    assertThat("Height setting failed", canvas.getHeight(), is(4));
  }
  @Test
  public void shouldColorAPoint(){
    canvas.setPixel(point(1,1), 'o');
    assertThat(canvas.getPixel(point(1,1)), is('o'));
  }

  @Test
  public void shouldPrint() {
    String expected = testData("canvas-20x4.txt");
    StringBuffer buffer = new StringBuffer(expected.length());

    canvas.render(buffer::append);
    String actual = buffer.toString();

    assertNotNull("Output not rendered", actual);
    assertTrue("Output is empty", actual.length() > 0);
    assertThat("Unexpected output ofr 20x4 canvas", actual, is(expected));

  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingZeroZeroPixel(){
    canvas.setPixel(point(0,0),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingNegativeXPixel(){
    canvas.setPixel(point(-1,1),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingNegativeYPixel(){
    canvas.setPixel(point(1,-1),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingNegativeXYPixel(){
    canvas.setPixel(point(-1,-1),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingPixelOutsideX(){
    canvas.setPixel(point(21,4),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingPixelOutsideY(){
    canvas.setPixel(point(20,5),'o');
  }
  @Test(expected = CanvasException.class)
  public void failsWhenSettingPixelOutsideXY(){
    canvas.setPixel(point(21,5),'o');
  }

}
