package me.yogendra.app.draw;

import static me.yogendra.app.draw.Point.point;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class PointTest {

  @Test
  public void shouldCreateWithNumbers(){
    Point point = point(0,0);
    assertThat(point, notNullValue());
  }

  @Test
  public void shouldEquateToAnotherPoint(){
    Point point1 = point(0,0);
    Point point2 = point(0,0);
    assertEquals(point1, point2);
  }
  @Test
  public void failsToEquateWithOtherObjects(){
    Point point1 = point(0,0);
    assertFalse(point1.equals(""));
  }
  @Test
  public void failsToEquateIfXAreDifferent(){
    Point point1 = point(0,0);
    Point point2 = point(1,0);
    assertNotEquals(point1, point2);
  }

  @Test
  public void failsToEquateIfYAreDifferent(){
    Point point1 = point(0,0);
    Point point2 = point(0,1);
    assertNotEquals(point1, point2);
  }
  @Test
  public void failsToEquateIfXYAreDifferent(){
    Point point1 = point(0,0);
    Point point2 = point(1,1);
    assertNotEquals(point1, point2);
  }
  @Test
  public void shouldProvideString(){
    Point point = point(0,0);
    assertThat(point.toString(), is("(0,0)"));
  }
  @Test
  public void shouldHaveDifferentHashCode(){
    Point point1 = point(1,2);
    Point point2 = point(2, 1);
    assertThat(point1.hashCode(), not(point2.hashCode()));
  }

}