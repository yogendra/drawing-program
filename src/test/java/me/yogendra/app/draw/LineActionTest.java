package me.yogendra.app.draw;

import static me.yogendra.app.TestHelper.mockCanvas5x5;
import static me.yogendra.app.draw.Point.point;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class LineActionTest {

  @Test
  public void shouldCreate() {
    LineAction action = new LineAction(point(1, 1), point(5, 1));
    assertThat(action, notNullValue());
  }

  @Test(expected = CanvasException.class)
  public void failsWhenStartIsMissing() {
    new LineAction(null, point(1, 1));
  }

  @Test(expected = CanvasException.class)
  public void failsWhenEndIsMissing() {
    new LineAction(point(1, 1), null);
  }

  @Test(expected = CanvasException.class)
  public void failsToRenderSlantedLines() {
    LineAction action = new LineAction(point(1, 1), point(5, 5));
    assertThat(action, notNullValue());
    Canvas canvas = mockCanvas5x5();
    action.executeOn(canvas);
  }

  @Test
  public void shouldDrawHorizontalLine() {
    LineAction action = new LineAction(point(1, 3), point(5, 3));
    assertThat(action, notNullValue());
    Canvas canvas = mockCanvas5x5();
    action.executeOn(canvas);
    verify(canvas).setPixel(point(1, 3), 'x');
    verify(canvas).setPixel(point(2, 3), 'x');
    verify(canvas).setPixel(point(3, 3), 'x');
    verify(canvas).setPixel(point(4, 3), 'x');
    verify(canvas).setPixel(point(5, 3), 'x');
  }

  @Test
  public void shouldDrawVerticalLine() {
    LineAction action = new LineAction(point(3, 1), point(3, 5));
    assertThat(action, notNullValue());
    Canvas canvas = mockCanvas5x5();
    action.executeOn(canvas);
    verify(canvas).setPixel(point(3, 1), 'x');
    verify(canvas).setPixel(point(3, 2), 'x');
    verify(canvas).setPixel(point(3, 3), 'x');
    verify(canvas).setPixel(point(3, 4), 'x');
    verify(canvas).setPixel(point(3, 5), 'x');
  }
}