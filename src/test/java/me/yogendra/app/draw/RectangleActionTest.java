package me.yogendra.app.draw;

import static me.yogendra.app.TestHelper.mockCanvas5x5;
import static me.yogendra.app.draw.Point.point;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import me.yogendra.app.TestHelper;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.verification.VerificationMode;

public class RectangleActionTest {

  @Test
  public void shouldCreate(){
    Point start = point(1, 1);
    Point end = point(2,2);

    RectangleAction action = new RectangleAction(start, end);
    assertThat(action, notNullValue());
  }

  @Test(expected = CanvasException.class)
  public void failsWhenStartIsMissing(){
    new RectangleAction(null, point(1,1));
  }
  @Test(expected = CanvasException.class)
  public void failsWhenEndIsMissing(){
    new RectangleAction(point(1,1), null);
  }

  @Test
  public void shouldRenderToRectangle(){
    Point start = point(2, 2);
    Point end = point(4,4);

    RectangleAction action = new RectangleAction(start, end);
    Canvas canvas = mockCanvas5x5();
    action.executeOn(canvas);

    VerificationMode twice = times(2);
    verify(canvas, twice).setPixel(point(2,2), 'x');
    verify(canvas, twice).setPixel(point(2,4), 'x');
    verify(canvas, twice).setPixel(point(4,2), 'x');
    verify(canvas, twice).setPixel(point(4,4), 'x');
    verify(canvas).setPixel(point(3,2), 'x');
    verify(canvas).setPixel(point(3,4), 'x');
    verify(canvas).setPixel(point(2,3), 'x');
    verify(canvas).setPixel(point(4,3), 'x');
  }

}