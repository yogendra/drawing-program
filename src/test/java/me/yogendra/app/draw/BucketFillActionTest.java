package me.yogendra.app.draw;

import static me.yogendra.app.TestHelper.mockCanvas;
import static me.yogendra.app.draw.Point.point;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.slf4j.LoggerFactory.getLogger;

import java.util.stream.IntStream;
import org.junit.Test;
import org.slf4j.Logger;

public class BucketFillActionTest {

  private static final Logger logger = getLogger(BucketFillActionTest.class);

  public static final char EMPTY = 0;
  private BucketFillAction action;

  @Test
  public void shouldCreateWithPointAndColor() {
    this.action = new BucketFillAction(point(1, 1), 'c');
    assertThat(action, notNullValue());
  }

  @Test(expected = CanvasException.class)
  public void failsToCreateWithourPoint() {
    new BucketFillAction(null, EMPTY);
  }

  @Test(expected = CanvasException.class)
  public void failsToCreateWithoutColor() {
    new BucketFillAction(point(1, 1), EMPTY);
  }

  @Test
  public void shouldPaintEmptyCanvas() {
    action = new BucketFillAction(point(1, 1), 'o');
    Canvas canvas = mockCanvas(new char[][]{
        {' ', ' '},
        {' ', ' '}
    });
    action.executeOn(canvas);
    verify(canvas, times(4)).setPixel(any(Point.class), any(Character.class));
  }

  @Test
  public void shouldNotPaintDifferentColoredPixel() {
    action = new BucketFillAction(point(1, 1), 'o');
    Canvas canvas = mockCanvas(new char[][]{
        {' ', ' '},
        {' ', '#'}
    });
    action.executeOn(canvas);
    verify(canvas, never()).setPixel(point(2, 2), 'o');
  }

  @Test
  public void shouldNotPaintDifferentColoredPixel2() {
    action = new BucketFillAction(point(2, 2), 'o');
    Canvas canvas = mockCanvas(new char[][]{
        {' ', ' '},
        {' ', '#'}
    });
    action.executeOn(canvas);
    verify(canvas, never()).setPixel(point(1, 1), 'o');
    verify(canvas, never()).setPixel(point(1, 2), 'o');
    verify(canvas, never()).setPixel(point(2, 1), 'o');
    verify(canvas).setPixel(point(2, 2), 'o');
  }

  @Test
  public void shouldNotPaintUnreachablePixels() {
    action = new BucketFillAction(point(1, 1), 'o');

    Canvas canvas = mockCanvas(new char[][]{
        {' ', ' ', ' '},
        {'#', '#', '#'},
        {' ', ' ', ' '},
    });

    action.executeOn(canvas);

    verify(canvas).setPixel(point(1, 1), 'o');
    verify(canvas).setPixel(point(2, 1), 'o');
    verify(canvas).setPixel(point(3, 1), 'o');
    verify(canvas, never()).setPixel(point(1, 2), 'o');
    verify(canvas, never()).setPixel(point(2, 2), 'o');
    verify(canvas, never()).setPixel(point(3, 2), 'o');
    verify(canvas, never()).setPixel(point(1, 3), 'o');
    verify(canvas, never()).setPixel(point(2, 3), 'o');
    verify(canvas, never()).setPixel(point(3, 3), 'o');

  }


  @Test
  public void shouldPaintAroundObstruction() {
    action = new BucketFillAction(point(1, 1), 'o');
    Canvas canvas = mockCanvas(new char[][]{
        {' ', ' ', ' '},
        {'#', ' ', ' '},
        {' ', ' ', ' '}
    });

    action.executeOn(canvas);

    verify(canvas).setPixel(point(1, 1), 'o');
    verify(canvas).setPixel(point(2, 1), 'o');
    verify(canvas).setPixel(point(3, 1), 'o');

    verify(canvas, never()).setPixel(point(1, 2), 'o');
    verify(canvas).setPixel(point(2, 2), 'o');
    verify(canvas).setPixel(point(3, 2), 'o');

    verify(canvas).setPixel(point(1, 3), 'o');
    verify(canvas).setPixel(point(2, 3), 'o');
    verify(canvas).setPixel(point(3, 3), 'o');
  }


}