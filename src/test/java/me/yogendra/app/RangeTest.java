package me.yogendra.app;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.stream.IntStream;
import org.junit.Test;

public class RangeTest {

  @Test
  public void ascending(){
    int [] expected = {0,1,2,3,4};
    int [] actual = IntStream.range(0, 5).toArray();
    assertThat(actual, is(expected));
  }
  @Test
  public void descending(){
    int [] expected = {};
    int [] actual = IntStream.range(5, 0).toArray();
    assertThat(actual, is(expected));
  }
}
