package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class RectangleCommandTest {

  private RectangleCommand command;

  @Before
  public void setUp() {
    command = new RectangleCommand();
  }

  @Test
  public void shouldCreateRectangleAction(){
    CanvasAction action = command.execute("4 4 3 3");
    assertThat(action, notNullValue());
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenNoParamsGiven(){
    command.execute("");
  }
}