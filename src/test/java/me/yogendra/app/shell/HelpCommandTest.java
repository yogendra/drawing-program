package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.mockito.ArgumentCaptor;

public class HelpCommandTest {

  @Test
  public void shouldPrintHelp(){
    ShellProcessor shell = mock(ShellProcessor.class);
    HelpCommand c = new HelpCommand();
    ShellAction a = c.execute("");
    a.executeOn(shell);
    ArgumentCaptor<String> captor = ArgumentCaptor.forClass(String.class);
    verify(shell).print(captor.capture());
    assertThat(captor.getValue(), notNullValue());
  }


}