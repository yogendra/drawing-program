package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import me.yogendra.app.draw.Canvas;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class CreateCanvasCommandTest {

  private CreateCanvasCommand command;

  @Before
  public void setUp() {
    command = new CreateCanvasCommand();
  }

  @Test
  public void shouldShellCanvasAction(){
    ShellAction action = command.execute("20 4");
    assertThat(action , notNullValue());
  }

  @Test
  public void shouldCreateNewCanvas(){
    ShellAction action = command.execute("20 4");
    assertThat(action , notNullValue());
    ShellProcessor processor = mock(ShellProcessor.class);
    action.executeOn(processor);
    verify(processor).setCanvas(Mockito.any(Canvas.class));
  }


  @Test(expected = ShellProcessingException.class)
  public void failsWhenYIsMissing(){
    command.execute("20");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenNoParameterGiven(){
    command.execute("");
  }

}