package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import me.yogendra.app.draw.Point;
import org.junit.Test;

public class ParameterHelperTest {

  private ParameterHelper helper;

  @Test(expected = ShellProcessingException.class)
  public void shouldFailIfNullParametersGiven(){
    this.helper = new ParameterHelper(null);
  }
  @Test
  public void shouldReturnPositiveNumber(){
    this.helper = new ParameterHelper("1");
    int i = helper.readPositiveNumber();
    assertThat(i, is(1));
  }

  @Test
  public void shouldReturnPoint(){
    this.helper = new ParameterHelper("1 1");
    Point p = helper.readPoint();
    assertThat(p, notNullValue());
    assertThat(p.x, is(1));
    assertThat(p.y, is(1));
  }

  @Test
  public void shouldReturnColor(){
    this.helper = new ParameterHelper("c");
    char color = helper.readColor();
    assertThat(color, is('c'));
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenEmptyInputGivenForColor(){
    this.helper = new ParameterHelper("");
    helper.readColor();
  }
  @Test(expected = ShellProcessingException.class)
  public void failsWhenColorCodeIfLonger(){
    this.helper = new ParameterHelper("yellow");
    helper.readColor();
  }

  @Test(expected = ShellProcessingException.class)
  public void failWhenOneNumberIsGivenForPoint(){
    this.helper = new ParameterHelper("1");
    helper.readPoint();
  }

  @Test(expected = ShellProcessingException.class)
  public void failWhenNegativeNumberIsGiven(){
    this.helper = new ParameterHelper("-1");
    helper.readPositiveNumber();
  }

  @Test(expected = ShellProcessingException.class)
  public void failWhenNegativeNumberIsGiveForPoint(){
    this.helper = new ParameterHelper("1 -1");
    helper.readPoint();
  }
}