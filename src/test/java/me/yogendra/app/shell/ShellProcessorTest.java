package me.yogendra.app.shell;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.function.Consumer;
import me.yogendra.app.draw.Canvas;
import org.junit.Before;
import org.junit.Test;

public class ShellProcessorTest {

  static final String QUIT = "Q";
  static final String CANVAS = "C";
  static final String LINE = "L";
  private Consumer<String> out;
  private ShellProcessor processor;


  private ShellAction quitAction;
  private Command<ShellAction> quitCommand;

  private ShellAction createCanvasAction;
  private Command<ShellAction> createCanvasCommand;

  private CanvasAction lineAction;
  private Command<CanvasAction> lineCommand;
  private Canvas canvas;
  private boolean shouldContinue;


  @Before
  @SuppressWarnings("unchecked")
  public void setUp() {
    out = mock(Consumer.class);

    canvas = new Canvas(10,10);

    quitCommand = mock(Command.class);
    createCanvasCommand = mock(Command.class);

    quitAction = ShellProcessor::shutdown;
    createCanvasAction = shellProcessor -> shellProcessor.setCanvas(canvas);

    lineAction = mock(CanvasAction.class);
    lineCommand = mock(Command.class);

    when(quitCommand.getName()).thenReturn(QUIT);
    when(quitCommand.execute(anyString())).thenReturn(quitAction);

    when(createCanvasCommand.getName()).thenReturn(CANVAS);
    when(createCanvasCommand.execute(anyString())).thenReturn(createCanvasAction);

    when(lineCommand.getName()).thenReturn(LINE);
    when(lineCommand.execute(anyString())).thenReturn(lineAction);

    processor = new ShellProcessor(out, quitCommand, createCanvasCommand, lineCommand);

  }

  @Test
  public void shouldReturnFalseOnQuitCommand(){
    shouldContinue = processor.execute(QUIT);
    assertFalse(shouldContinue);
  }

  @Test
  public void sendsReturnTrueNonQuitCommand(){
    shouldContinue = processor.execute(CANVAS);
    assertTrue(shouldContinue);
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenCommandIsEmpty(){
    processor.execute("");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenUnknownCommandGiven(){
    processor.execute("monkey kong");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsExecutingAfterShutdown(){
    shouldContinue = processor.execute(QUIT);
    assertFalse(shouldContinue);
    processor.execute(CANVAS);
  }
  @Test
  public void shouldExecuteCanvasAction(){
    shouldContinue = processor.execute(CANVAS);
    assertTrue(shouldContinue);
    shouldContinue = processor.execute(LINE);
    assertTrue(shouldContinue);
    verify(lineAction).executeOn(canvas);
  }
  @Test
  public void shouldExecuteShellAction(){
    Command<ShellAction> command = mock(Command.class);

    ShellAction action = mock(ShellAction.class);

    when(command.execute(anyString())).thenReturn(action);
    when(command.getName()).thenReturn(CANVAS);

    processor = new ShellProcessor(out, command);

    shouldContinue = processor.execute(CANVAS);

    verify(action).executeOn(processor);
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenCanvasActionExecutedBeforeCreatingCanvas(){
    shouldContinue = processor.execute(LINE);
  }

  @Test
  public void shouldPrintToOutput(){
    processor.print("Hello");
    verify(out).accept("Hello");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenEncoutersUnknowAction(){
    ShellProcessor processor = new ShellProcessor(out, new FooCommand());
    processor.execute("F");
  }
  static class FooAction implements Action<String>{

    @Override
    public void executeOn(String s) {
      fail("This should not");
    }
  }
  static class FooCommand implements Command<FooAction>{
    @Override
    public String getName() {
      return "F";
    }

    @Override
    public FooAction execute(String params) {
      return new FooAction();
    }
  }
}
