package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class LineCommandTest {
  LineCommand command;
  @Before
  public void setUp(){
    this.command = new LineCommand();
  }

  @Test
  public void shouldCreateCanvasAction(){
    CanvasAction action = command.execute("1 6 4 6 ");
    assertThat(action, notNullValue());
  }
  @Test(expected = ShellProcessingException.class)
  public void failsWhenNoParamsProvided(){
    command.execute("");
  }
  @Test(expected = ShellProcessingException.class)
  public void failsWhenOneNumberGiven(){
    command.execute("1");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenTwoNumbersGiven(){
    command.execute("1 6");
  }
  @Test(expected = ShellProcessingException.class)
  public void failsWhenThreeNumbersGiven(){
    command.execute("1 6 4");
  }
  @Test(expected = ShellProcessingException.class)
  public void faileWhenTextGiven(){
    command.execute("foo");
  }

}