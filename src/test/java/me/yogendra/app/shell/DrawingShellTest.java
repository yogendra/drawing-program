package me.yogendra.app.shell;

import static me.yogendra.app.shell.DrawingShell.PROMPT;
import static me.yogendra.app.shell.ShellProcessingException.commandNotFound;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;


public class DrawingShellTest {

  private DrawingShell shell;
  private Supplier<String> in;
  private Consumer<String> out;
  private ShellProcessor processor;
  private String quitCommand;

  @Before
  @SuppressWarnings("unchecked")
  public void setUp() {

    in = mock(Supplier.class);
    out = mock(Consumer.class);
    processor = mock(ShellProcessor.class);
    shell = new DrawingShell(in, processor, out);
    quitCommand = "quit";

  }

  @Test
  public void ok() {
    assertNotNull(shell);
  }

  @Test
  public void shouldPrintPrompt() {
    when(in.get()).thenReturn(quitCommand);
    when(processor.execute(quitCommand)).thenReturn(false);

    shell.start();

    verify(out).accept(DrawingShell.PROMPT);
  }

  @Test
  public void printsErrorIfCommandFails() {
    String missingMockCommand = "mockcmd";
    String exceptionMessage = "Command not found";
    String expected = String.format("Error in executing %s: %s%n", missingMockCommand, exceptionMessage);

    when(in.get()).thenReturn(missingMockCommand).thenReturn(quitCommand);
    when(processor.execute(missingMockCommand))
        .thenThrow(commandNotFound());

    when(processor.execute(quitCommand)).thenReturn(false);

    shell.start();

    ArgumentCaptor<String> outputCapture = ArgumentCaptor.forClass(String.class);
    verify(out, times(3)).accept(outputCapture.capture());

    List<String> messages = outputCapture.getAllValues();
    assertThat(messages, notNullValue());
    assertThat(messages.size(), is(3));
    assertThat(messages.get(0), is(PROMPT));
    assertThat(messages.get(1), is(expected));
    assertThat(messages.get(2), is(PROMPT));
  }

  @Test
  public void shouldQuitsWhenProcessorReturnsFalse() {
    when(in.get()).thenReturn(quitCommand);
    when(processor.execute(quitCommand)).thenReturn(false);

    shell.start();

    verify(out, only()).accept(PROMPT);


  }
}
