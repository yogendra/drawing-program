package me.yogendra.app.shell;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class BucketFillCommandTest {

  private BucketFillCommand command;

  @Before
  public void setUp() {
    command = new BucketFillCommand();
  }

  @Test
  public void shouldCreateBucketFillAction() {
    assertThat(command.execute("1 1 c"), notNullValue());
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenColorArgumentMissing(){
    command.execute("1 1");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenArgumentEmpty(){
    command.execute("");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenArgumentNull(){
    command.execute(null);
  }
  @Test(expected = ShellProcessingException.class)
  public void failsWhenArgumentsAreInWrongOrder(){
    command.execute("c 1 1");
  }

  @Test(expected = ShellProcessingException.class)
  public void failsWhenPointNotGiven(){
    command.execute("c");
  }
}