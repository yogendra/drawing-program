package me.yogendra.app;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import me.yogendra.app.draw.Canvas;
import me.yogendra.app.draw.Point;
import org.junit.Test;

public class TestHelper {

  public static String testData(String name) {
    try {
      URL resource = TestHelper.class.getClassLoader().getResource(name);
      Path path = Paths.get(requireNonNull(resource).toURI());
      return new String(Files.readAllBytes(path));
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException(name + ": Unable to load file from classpath", e);
    } catch (IOException e1) {
      throw new IllegalArgumentException("Error reading file", e1);
    }
  }

  public static Canvas mockCanvas(char[][] view) {
    Canvas canvas = mock(Canvas.class);

    when(canvas.getHeight()).thenReturn(view.length);
    when(canvas.getWidth()).thenReturn(view[0].length);

    when(canvas.getPixel(any(Point.class))).then(call -> {
      Point p = call.getArgument(0);
      return view[p.y - 1][p.x - 1];
    });
    return canvas;
  }
  public static Canvas mockCanvas5x5(){
    return mockCanvas(new char[][]{
        {' ',' ',' ',' ',' '},
        {' ',' ',' ',' ',' '},
        {' ',' ',' ',' ',' '},
        {' ',' ',' ',' ',' '},
        {' ',' ',' ',' ',' '}
    });
  }

  @Test
  public void readFileFromClasspath() {
    String actual = testData("test-helper.txt");
    assertNotNull(actual);
    assertThat(actual, is("Test Helper Success"));
  }

}
