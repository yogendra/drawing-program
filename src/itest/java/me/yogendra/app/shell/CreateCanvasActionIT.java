package me.yogendra.app.shell;

import static me.yogendra.app.draw.TestHelper.testData;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CreateCanvasActionIT {

  private ShellProcessor processor;
  private StringBuilder buffer;


  @Before
  public void setUp() {
    buffer = new StringBuilder(22 * 6);
    processor = new ShellProcessor(buffer::append, new CreateCanvasCommand(), new RectangleCommand(), new LineCommand(), new BucketFillCommand(), new HelpCommand(), new QuitCommand());
  }

  @Test
  public void fillsCreatesCanvas() {
    String expected = testData("data/empty-canvas.txt");
    boolean result = processor.execute("C 20 4");
    String actual = buffer.toString();
    assertThat(actual, is(expected));
  }
  @Test
  public void sampleActions(){
    String expected = testData("data/req001.txt");
    boolean proceed = processor.execute("C 20 4");
    assertTrue(proceed);
    proceed = processor.execute("L 1 2 6 2");
    assertTrue(proceed);
    proceed = processor.execute("L 6 3 6 4");
    assertTrue(proceed);
    proceed = processor.execute("R 14 1 18 3");
    assertTrue(proceed);
    proceed = processor.execute("B 10 3 o");
    assertTrue(proceed);
    proceed = processor.execute("Q");
    assertFalse(proceed);
    String actual = buffer.toString();
    assertThat(actual, is(expected));
  }
}
