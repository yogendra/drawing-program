package me.yogendra.app.draw;

import static me.yogendra.app.draw.Point.point;
import static me.yogendra.app.draw.TestHelper.testData;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class LineActionIT {

  private LineAction action;
  private Canvas canvas;
  private StringBuilder buffer;


  @Before
  public void setUp() {
    canvas = new Canvas(20, 4);
    buffer = new StringBuilder(22 * 6);
  }

  @Test
  public void createsWithPoints() {
    Point start = point(1, 1);
    Point end = point(1, 2);
    action = new LineAction(start, end);

    assertThat(action, notNullValue());
  }

  @Test
  public void invokesCanvas() {
    canvas = mock(Canvas.class);

    Point start = point(1, 1);
    Point end = point(1, 2);
    action = new LineAction(start, end);
    action.executeOn(canvas);

    verify(canvas, times(2)).setPixel(Mockito.any(Point.class), Mockito.eq('x'));


  }

  @Test
  public void rendersHorizontalLine() {
    String expected = testData("data/line/test-1.txt");

    action = new LineAction(point(1, 2), point(6, 2));
    action.executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();

    assertThat(actual, is(expected));
  }

  @Test
  public void renderVerticalLine() {
    String expected = testData("data/line/test-2.txt");

    action = new LineAction(point(6, 3), point(6, 4));
    action.executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();

    assertThat(actual, is(expected));
  }

  @Test
  public void renderVerticalAndHorizontalLine() {
    String expected = testData("data/line/test-3.txt");

    action = new LineAction(point(1, 2), point(6, 2));
    action.executeOn(canvas);
    action = new LineAction(point(6, 3), point(6, 4));
    action.executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();

    assertThat(actual, is(expected));
  }

  @Test(expected = CanvasException.class)
  public void throwsExceptionWhenFeatureNotSupported(){
    action = new LineAction(point(1,1), point(2,3));
    action.executeOn(canvas);
  }


}