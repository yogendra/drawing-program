package me.yogendra.app.draw;

import static me.yogendra.app.draw.Point.point;
import static me.yogendra.app.draw.TestHelper.testData;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.slf4j.LoggerFactory.getLogger;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;

public class BucketFillActionIT {
  private static final Logger logger = getLogger(BucketFillActionIT.class);


  private Canvas canvas;
  private StringBuilder buffer;


  @Before
  public void setUp() {
    canvas = new Canvas(20, 4);
    buffer = new StringBuilder(22 * 6);
  }

  @Test
  public void fillsEmptyCanvas(){
    String expected = testData("data/bucket/test-1.txt");
    BucketFillAction action = new BucketFillAction(point(1,1), 'c');
    action.executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();
    logger.debug("Canvas: \n{}", actual);
    assertThat(actual, is(expected));
  }

  @Test
  public  void fillCanvasWithContent(){
    String expected = testData("data/bucket/test-2.txt");
    new LineAction(point(1,2), point(6,2)).executeOn(canvas);
    new LineAction(point(6,3), point(6,4)).executeOn(canvas);
    new RectangleAction(point(14,1), point(18,3)).executeOn(canvas);
    new BucketFillAction(point(10,3), 'o').executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();
    logger.debug("Canvas: \n{}", actual);
    assertThat(actual, is(expected));

  }


}