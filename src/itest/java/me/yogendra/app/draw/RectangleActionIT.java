package me.yogendra.app.draw;

import static me.yogendra.app.draw.Point.point;
import static me.yogendra.app.draw.TestHelper.testData;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RectangleActionIT {

  private Canvas canvas;
  private RectangleAction action;
  private StringBuilder buffer;

  @Before
  public void setUp(){
    buffer = new StringBuilder(22*6);
    canvas = new Canvas(20, 4);
  }

  @Test
  public void renderRectangle(){
    String expected = testData("data/rectangle/test-1.txt");

    action = new RectangleAction(point(14, 1), point(18,3));
    action.executeOn(canvas);
    canvas.render(buffer::append);
    String actual = buffer.toString();

    assertThat(actual, is(expected));

  }
}