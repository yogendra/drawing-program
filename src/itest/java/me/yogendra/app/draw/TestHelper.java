package me.yogendra.app.draw;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import org.junit.Test;

public class TestHelper {

  public static String testData(String name) {
    try {
      URL resource = TestHelper.class.getClassLoader().getResource(name);
      Path path = Paths.get(requireNonNull(resource).toURI());
      return new String(Files.readAllBytes(path));
    } catch (URISyntaxException e) {
      throw new IllegalArgumentException(name + ": Unable to load file from classpath", e);
    } catch (IOException e1) {
      throw new IllegalArgumentException("Error reading file", e1);
    }
  }
  @Test
  public void readFileFromClasspath() {
    String actual = testData("test-helper.txt");
    assertNotNull(actual);
    assertThat(actual, is("Test Helper Success"));
  }

}
